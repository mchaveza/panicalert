package com.example.mchaveza.panicalert;

import android.app.Application;
import android.os.PowerManager;

import com.example.mchaveza.panicalert.ui.utils.LifeCycleHandler;
import com.facebook.stetho.Stetho;

/**
 * Created by mchaveza on 14/08/2017.
 */

public class PanicApplication extends Application {

    private PowerManager.WakeLock wakeLock;

    @Override
    public void onCreate() {
        super.onCreate();

        PowerManager pm = (PowerManager) getSystemService(this.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNotSleep");

        Stetho.initializeWithDefaults(this);
        registerActivityLifecycleCallbacks(new LifeCycleHandler(getApplicationContext()));
    }

}
