package com.example.mchaveza.panicalert.data.model;

import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by mchaveza on 16/08/2017.
 */

public class FirebaseUserModel {

    public LatLng userCoords;
    public String name;
    public String status;
    public String address;
    public String safeStatus;
    public String key;
    public String uri;

    public LatLng getUserCoords() {
        return userCoords;
    }

    public void setUserCoords(LatLng userCoords) {
        this.userCoords = userCoords;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSafeStatus() {
        return safeStatus;
    }

    public void setSafeStatus(String safeStatus) {
        this.safeStatus = safeStatus;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
