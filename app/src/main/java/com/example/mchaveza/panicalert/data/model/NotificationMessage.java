package com.example.mchaveza.panicalert.data.model;

/**
 * Created by mchaveza on 05/09/2017.
 */

public class NotificationMessage {

    String title;
    String message;

    public NotificationMessage() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
