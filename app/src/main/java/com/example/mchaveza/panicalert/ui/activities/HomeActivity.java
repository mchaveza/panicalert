package com.example.mchaveza.panicalert.ui.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.ui.bases.BaseActivity;
import com.example.mchaveza.panicalert.ui.fragments.HomeFragment;
import com.example.mchaveza.panicalert.ui.utils.ArgumentsConstants;

public class HomeActivity extends BaseActivity {

    /** INSTANCES OF CLASSES **/

    /**
     * SYSTEM VARIABLES
     **/
    private boolean locationPermissionGranted;
    private boolean readStorageGranted;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, HomeFragment.newInstance())
                    .commit();
        }

        setupScreen(true, getString(R.string.home), false);

        locationPermissionGranted = sharedPreferences.getBooleanPreference(ArgumentsConstants.PARAM_LOCATION_GRANTED, false);
        readStorageGranted = sharedPreferences.getBooleanPreference(ArgumentsConstants.PARAM_READ_STORAGE_GRANTED, false);

        if (!locationPermissionGranted || !readStorageGranted) {
            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true;
                    sharedPreferences.setBooleanPreference(ArgumentsConstants.PARAM_LOCATION_GRANTED, locationPermissionGranted);
                } else {
                    locationPermissionGranted = false;
                    sharedPreferences.setBooleanPreference(ArgumentsConstants.PARAM_LOCATION_GRANTED, locationPermissionGranted);
                    Toast.makeText(HomeActivity.this, this.getString(R.string.home_location_permission_message), Toast.LENGTH_SHORT).show();
                }
                if (grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    readStorageGranted = true;
                    sharedPreferences.setBooleanPreference(ArgumentsConstants.PARAM_READ_STORAGE_GRANTED, readStorageGranted);
                } else {
                    readStorageGranted = false;
                    sharedPreferences.setBooleanPreference(ArgumentsConstants.PARAM_READ_STORAGE_GRANTED, readStorageGranted);
                    Toast.makeText(HomeActivity.this, this.getString(R.string.home_read_permission_message), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
}
