package com.example.mchaveza.panicalert.ui.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.ui.dialogs.DialogDefaultBuilder;
import com.example.mchaveza.panicalert.ui.utils.ArgumentsConstants;
import com.example.mchaveza.panicalert.ui.utils.SharedPreferencesConfiguration;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

/**
 * Created by mchaveza on 14/08/2017.
 */

public class IntroActivity extends AppIntro {

    private DialogDefaultBuilder dialogDefaultBuilder;
    private SharedPreferencesConfiguration sharedPreferences;
    private boolean isFirstTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = new SharedPreferencesConfiguration(this);
        isFirstTime = sharedPreferences.getBooleanPreference(ArgumentsConstants.PARAM_FIRST_TIME, true);

        if (isFirstTime) {
            dialogDefaultBuilder = new DialogDefaultBuilder(this);
            addSlide(AppIntroFragment.newInstance(getString(R.string.intro_dialog_title1), getString(R.string.intro_dialog_message1), R.drawable.ic_tuto_1, getResources().getColor(R.color.primary_dark)));
            addSlide(AppIntroFragment.newInstance(getString(R.string.intro_dialog_title2), getString(R.string.intro_dialog_message2), R.drawable.ic_tuto_2, getResources().getColor(R.color.cpb_blue)));
            addSlide(AppIntroFragment.newInstance(getString(R.string.intro_dialog_title3), getString(R.string.intro_dialog_message3), R.drawable.ic_tuto_3, getResources().getColor(R.color.cpb_green_dark)));
            showSkipButton(true);
            setProgressButtonEnabled(true);
        } else {
            finish();
        }
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        dialogDefaultBuilder.buildTwoButtonDialog(getString(R.string.intro_dialog_title_leave_tutorial), getString(R.string.intro_dialog_message_leave_tutorial), new DialogDefaultBuilder.AlertAction() {
            @Override
            public void onAccept(DialogInterface dialog, int id) {
                Intent goToHome = new Intent(IntroActivity.this, HomeActivity.class);
                startActivity(goToHome);
                finish();
            }

            @Override
            public void onDeny(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });


    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent goToHome = new Intent(IntroActivity.this, HomeActivity.class);
        startActivity(goToHome);
        finish();
    }
}
