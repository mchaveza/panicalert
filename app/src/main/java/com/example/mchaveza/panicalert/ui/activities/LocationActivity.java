package com.example.mchaveza.panicalert.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.ui.bases.BaseActivity;
import com.example.mchaveza.panicalert.ui.fragments.LocationFragment;

/**
 * Created by mchaveza on 14/08/2017.
 */

public class LocationActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String title = null;
        String message = null;

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                if (key.equals("title")) {
                    title = (String) getIntent().getExtras().get(key);
                }
                if (key.equals("message")) {
                    message = (String) getIntent().getExtras().get(key);
                }
            }

        }

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, LocationFragment.newInstance(title, message))
                    .commit();
        }

        setupScreen(false, getString(R.string.location), false);
    }
}
