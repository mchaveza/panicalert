package com.example.mchaveza.panicalert.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.ui.bases.BaseActivity;
import com.example.mchaveza.panicalert.ui.fragments.SettingsFragment;

/**
 * Created by mchaveza on 14/08/2017.
 */

public class SettingsActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, SettingsFragment.newInstance())
                    .commit();
        }

        setupScreen(false, getString(R.string.settings), true);
    }
}
