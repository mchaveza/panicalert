package com.example.mchaveza.panicalert.ui.activities;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.ui.dialogs.DialogDefaultBuilder;
import com.example.mchaveza.panicalert.ui.utils.ArgumentsConstants;
import com.example.mchaveza.panicalert.ui.utils.Connectivity;
import com.example.mchaveza.panicalert.ui.utils.FirebaseMessaginService;
import com.example.mchaveza.panicalert.ui.utils.SharedPreferencesConfiguration;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mchaveza on 14/08/2017.
 */

public class SplashActivity extends AppCompatActivity {

    /**
     * BINDED RESOURCES
     **/
    @BindView(R.id.splash_logo)
    public ImageView ivSplash;

    /**
     * INSTANCES OF CLASSES
     **/
    private SharedPreferencesConfiguration sharedPreferences;
    private Connectivity connectivity;
    private DialogDefaultBuilder dialogDefaultBuilder;

    /**
     * SYSTEM VARIABLES
     **/
    boolean isFirstTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        ButterKnife.bind(this);

        connectivity = new Connectivity();
        sharedPreferences = new SharedPreferencesConfiguration(SplashActivity.this);
        dialogDefaultBuilder = new DialogDefaultBuilder(this);

        YoYo.with(Techniques.BounceIn)
                .duration(1750)
                .playOn(ivSplash);

        startTimer();

    }

    public void startTimer() {
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        if (connectivity.isConnected(SplashActivity.this)) {
                            moveToNextFragment();
                        } else {
                            dialogDefaultBuilder.buildOneButtonDialog(getString(R.string.splash_dialog_title_no_internet), getString(R.string.splash_dialog_message_no_internet), new DialogDefaultBuilder.SimpleAlertAction() {
                                @Override
                                public void onAccept(DialogInterface dialog, int id) {
                                    finish();
                                }
                            });
                        }
                    }
                },
                2000
        );
    }

    private void moveToNextFragment() {
        isFirstTime = sharedPreferences.getBooleanPreference(ArgumentsConstants.PARAM_FIRST_TIME, true);
        Intent nextFragment;
        if (isFirstTime) {
            nextFragment = new Intent(SplashActivity.this, IntroActivity.class);
            startActivity(nextFragment);
            finish();
        } else {
            nextFragment = new Intent(SplashActivity.this, HomeActivity.class);
        }
        FirebaseMessaginService.subscribeToTopic("notifications");
        startActivity(nextFragment);
        finish();
    }

}