package com.example.mchaveza.panicalert.ui.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.data.model.FirebaseUserModel;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mchaveza on 23/08/2017.
 */

public class RecyclerLocationAdapter extends RecyclerView.Adapter<RecyclerLocationAdapter.ItemViewHolder> {

    private Context mContext;
    private List<FirebaseUserModel> mList;
    private RecyclerLocationAdapter.onEventClickListener mListener;

    public RecyclerLocationAdapter(Context mContext, List<FirebaseUserModel> mList, onEventClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public RecyclerLocationAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_location, parent, false);

        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        final FirebaseUserModel model = mList.get(position);

        holder.tvName.setText(model.getName());
        holder.tvCurrentLocation.setText(model.getAddress());
        holder.tvStatus.setText(model.getStatus());
        holder.tvSafeStatus.setText(model.getSafeStatus());
        if (holder.tvSafeStatus.getText().toString().equals(mContext.getString(R.string.unsafe))) {
            holder.tvSafeStatus.setTextColor(mContext.getResources().getColor(R.color.primary_dark));
        }else{
            holder.tvSafeStatus.setTextColor(mContext.getResources().getColor(R.color.cpb_green_dark));
        }
        if (model.getUri() != null) {
            Glide.with(mContext)
                    .load(model.getUri())
                    .into(holder.ivProfile);
        }


        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClicked(model.getKey());
            }
        };

        holder.cvView.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_location_name)
        TextView tvName;

        @BindView(R.id.item_location_curloc)
        TextView tvCurrentLocation;

        @BindView(R.id.item_location_status)
        TextView tvStatus;

        @BindView(R.id.item_location_safe_status)
        TextView tvSafeStatus;

        @BindView(R.id.item_location_card)
        CardView cvView;

        @BindView(R.id.item_location_img)
        ImageView ivProfile;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onEventClickListener {

        void onItemClicked(String key);
    }
}
