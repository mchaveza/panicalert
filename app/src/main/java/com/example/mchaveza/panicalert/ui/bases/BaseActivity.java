package com.example.mchaveza.panicalert.ui.bases;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.ui.activities.SettingsActivity;
import com.example.mchaveza.panicalert.ui.utils.ArgumentsConstants;
import com.example.mchaveza.panicalert.ui.utils.SharedPreferencesConfiguration;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by mchaveza on 14/08/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    /**
     * BINDED RESOURCES
     **/
    @BindView(R.id.toolbar_go_back)
    ImageView ivGoBack;

    @BindView(R.id.toolbar_title)
    TextView tvTitle;

    @BindView(R.id.toolbar_settings)
    ImageView ivSettings;

    /**
     * INSTANCES OF CLASSES
     **/
    public SharedPreferencesConfiguration sharedPreferences;

    /**
     * SYSTEM VARIABLES
     **/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPreferencesConfiguration(this);
    }

    public void setupScreen(boolean hideGoBack, String sectionTitle, boolean hideSettings) {
        if (hideGoBack) {
            ivGoBack.setVisibility(View.GONE);
        }

        if (hideSettings) {
            ivSettings.setVisibility(View.GONE);
        }

        tvTitle.setText(sectionTitle);
    }

    @OnClick(R.id.toolbar_go_back)
    public void onGoBack() {
        finish();
    }

    @OnClick(R.id.toolbar_settings)
    public void onSettings() {
        Intent goToSettings = new Intent(getApplicationContext(), SettingsActivity.class);
        startActivity(goToSettings);
    }
}
