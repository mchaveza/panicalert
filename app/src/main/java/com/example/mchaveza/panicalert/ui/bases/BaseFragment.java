package com.example.mchaveza.panicalert.ui.bases;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.example.mchaveza.panicalert.data.model.FirebaseUserModel;
import com.example.mchaveza.panicalert.ui.dialogs.DialogDefaultBuilder;
import com.example.mchaveza.panicalert.ui.utils.ArgumentsConstants;
import com.example.mchaveza.panicalert.ui.utils.FirebaseDatabaseManager;
import com.example.mchaveza.panicalert.ui.utils.GPSTracker;
import com.example.mchaveza.panicalert.ui.utils.SharedPreferencesConfiguration;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;

/**
 * Created by mchaveza on 14/08/2017.
 */

public abstract class BaseFragment extends Fragment implements GPSTracker.onLocationHasChanged {

    public SharedPreferencesConfiguration sharedPreferences;
    public ArgumentsConstants args;
    public FirebaseDatabaseManager mDatabase;
    public GPSTracker gpsTracker;
    public DialogDefaultBuilder mDialog;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        sharedPreferences = new SharedPreferencesConfiguration(getActivity());
        args = new ArgumentsConstants();
        mDialog = new DialogDefaultBuilder(getActivity());

        mDatabase = FirebaseDatabaseManager.getInstance(getActivity());
        gpsTracker = new GPSTracker(getActivity(), this);
    }

    public void startBroadcasting(FirebaseUserModel model) {
        if (mDatabase.checkModel(model)) {
            mDatabase.broadcastInfo(model);
        }
    }

    @Override
    public void locationHasChanged(LatLng newLocation) {
        FirebaseUserModel mModel = new FirebaseUserModel();
        mModel.setName(sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_USER_NAME));
        mModel.setAddress(parseAddress(newLocation));
        mModel.setUserCoords(newLocation);

        startBroadcasting(mModel);
    }

    private String parseAddress(LatLng location) {
        Geocoder geocoder;
        List<Address> addresses;
        String address = "No disponible";
        try {
            geocoder = new Geocoder(getActivity().getApplicationContext(), Locale.getDefault());
            addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1);
            address = addresses.get(0).getAddressLine(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return address;
    }
}
