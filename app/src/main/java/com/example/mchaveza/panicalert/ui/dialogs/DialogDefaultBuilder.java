package com.example.mchaveza.panicalert.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.ui.activities.HomeActivity;

/**
 * Created by mchaveza on 14/08/2017.
 */

public class DialogDefaultBuilder {

    private Context mContext;

    public DialogDefaultBuilder(Context context) {
        mContext = context;
    }

    public AlertDialog.Builder buildOneButtonDialog(String title, String message, final SimpleAlertAction action) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(mContext.getString(R.string.accept), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        action.onAccept(dialog, which);
                    }
                })
                .create()
                .show();
        return alertDialogBuilder;
    }

    public AlertDialog.Builder buildTwoButtonDialog(String title, String message, final AlertAction action) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(mContext.getString(R.string.accept), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        action.onAccept(dialog, which);
                    }
                })
                .setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        action.onDeny(dialog, which);
                    }
                })
                .create()
                .show();
        return alertDialogBuilder;
    }

    public void buildToast(String message, int length) {
        Toast.makeText(mContext, message, length).show();
    }

    public void buildSnackbar(String message, View view) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    public interface SimpleAlertAction {

        void onAccept(DialogInterface dialog, int id);
    }

    public interface AlertAction {

        void onAccept(DialogInterface dialog, int id);

        void onDeny(DialogInterface dialog, int id);
    }

}
