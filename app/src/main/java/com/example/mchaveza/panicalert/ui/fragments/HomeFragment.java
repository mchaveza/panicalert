package com.example.mchaveza.panicalert.ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.example.mchaveza.panicalert.PanicApplication;
import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.data.model.FirebaseUserModel;
import com.example.mchaveza.panicalert.ui.activities.LocationActivity;
import com.example.mchaveza.panicalert.ui.activities.SettingsActivity;
import com.example.mchaveza.panicalert.ui.bases.BaseFragment;
import com.example.mchaveza.panicalert.ui.dialogs.DialogDefaultBuilder;
import com.example.mchaveza.panicalert.ui.utils.ArgumentsConstants;
import com.example.mchaveza.panicalert.ui.utils.FirebaseDatabaseManager;
import com.example.mchaveza.panicalert.ui.utils.FirebaseMessaginService;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by mchaveza on 14/08/2017.
 */

public class HomeFragment extends BaseFragment {

    /**
     * BINDED RESOURCES
     **/
    @BindView(R.id.home_radar)
    ImageView ivRadar;

    @BindView(R.id.home_alarm)
    ImageView ivAlarm;

    private CarouselView carouselView;

    /**
     * INSTANCES OF CLASSES
     **/
    public static HomeFragment instance;

    /**
     * SYSTEM VARIABLES
     **/
    private int[] carouselImages = {R.drawable.ic_cartel1, R.drawable.ic_cartel2, R.drawable.ic_cartel3, R.drawable.ic_cartel4, R.drawable.ic_cartel5};
    private boolean isFirstTime;


    public static HomeFragment newInstance() {
        if (instance == null) {
            instance = new HomeFragment();
        }
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDatabase.updateSingleItem(ArgumentsConstants.PARAM_USER_SAFE_STATUS, getString(R.string.safe));

        GlideDrawableImageViewTarget targetRadar = new GlideDrawableImageViewTarget(ivRadar);
        Glide.with(this)
                .load(R.drawable.radar)
                .into(targetRadar);

        GlideDrawableImageViewTarget targetAlarm = new GlideDrawableImageViewTarget(ivAlarm);
        Glide.with(this)
                .load(R.drawable.alarm)
                .into(targetAlarm);

        carouselView = (CarouselView) getActivity().findViewById(R.id.carousel);
        carouselView.setPageCount(carouselImages.length);

        carouselView.setImageListener(imageListener);

        isFirstTime = sharedPreferences.getBooleanPreference(ArgumentsConstants.PARAM_FIRST_TIME, true);
        if (isFirstTime) {
            isFirstTime = false;
            sharedPreferences.setBooleanPreference(ArgumentsConstants.PARAM_FIRST_TIME, isFirstTime);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle(getString(R.string.home_dialog_setup_title))
                    .setMessage(getString(R.string.home_dialog_setup_message))
                    .setPositiveButton(getActivity().getString(R.string.accept), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent goToClass = new Intent(getActivity().getApplicationContext(), SettingsActivity.class);
                            getActivity().startActivity(goToClass);
                        }
                    })
                    .create()
                    .show();
        }
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(carouselImages[position]);
        }
    };

    @OnClick(R.id.card_alarm)
    public void onAlarm() {
        FirebaseMessaginService mNotifications = new FirebaseMessaginService(getActivity());
        mNotifications.sendMessage(getString(R.string.alarm), sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_USER_MESSAGE, getString(R.string.settings_default_message)));
        mDatabase.updateSingleItem(ArgumentsConstants.PARAM_USER_SAFE_STATUS, getString(R.string.unsafe));
        mDialog.buildOneButtonDialog(getString(R.string.home_alert_title), getString(R.string.home_alert_message), new DialogDefaultBuilder.SimpleAlertAction() {
            @Override
            public void onAccept(DialogInterface dialog, int id) {
                mDatabase.deleteSingleItem("Notifications");
            }
        });
    }

    @OnClick(R.id.card_radar)
    public void onRadar() {
        Intent goToLocation = new Intent(getActivity(), LocationActivity.class);
        startActivity(goToLocation);
    }

}
