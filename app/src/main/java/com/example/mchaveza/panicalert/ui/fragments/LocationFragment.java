package com.example.mchaveza.panicalert.ui.fragments;

import android.location.Location;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.data.model.FirebaseUserModel;
import com.example.mchaveza.panicalert.ui.adapters.RecyclerLocationAdapter;
import com.example.mchaveza.panicalert.ui.bases.BaseFragment;
import com.example.mchaveza.panicalert.ui.utils.ArgumentsConstants;
import com.example.mchaveza.panicalert.ui.utils.FirebaseDatabaseManager;
import com.example.mchaveza.panicalert.ui.utils.MapsCustomizerManager;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;

public class LocationFragment extends BaseFragment implements FirebaseDatabaseManager.FirebaseCallback, RecyclerLocationAdapter.onEventClickListener {

    /**
     * BINDED RESOURCES
     **/
    private RecyclerView rvLocation;

    /**
     * INSTANCES OF CLASSES
     **/
    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private RecyclerLocationAdapter recyclerLocationAdapter;
    private MapsCustomizerManager mMapsCustomizerManager;
    private SlidingUpPanelLayout slidingLayout;

    /**
     * SYSTEM VARIABLES
     **/
    private String mapStyle;
    private String title;
    private String message;
    private Marker marker;
    private LatLng defaultPosition;
    private ArrayList<FirebaseUserModel> listModel = new ArrayList<>();
    private ArrayList<String> listIndex = new ArrayList<>();
    private ArrayList<Marker> listMarker = new ArrayList<>();


    public static LocationFragment newInstance(String title, String message) {
        LocationFragment fragment = new LocationFragment();
        if ((title != null) && (message != null)) {
            Bundle extras = new Bundle();
            extras.putString("title", title);
            extras.putString("message", message);
            fragment.setArguments(extras);
        }
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString("title");
            message = getArguments().getString("message");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_location, container, false);

        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        rvLocation = (RecyclerView) view.findViewById(R.id.location_recycler_locations);

        rvLocation.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerLocationAdapter = new RecyclerLocationAdapter(getActivity(), listModel, this);
        rvLocation.setAdapter(recyclerLocationAdapter);
        rvLocation.setHasFixedSize(true);

        slidingLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);

        rvLocation = (RecyclerView) view.findViewById(R.id.location_recycler_locations);
        mMapsCustomizerManager = new MapsCustomizerManager(getActivity());

        defaultPosition = new LatLng(19.702201, -101.190128);

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupMap();

        mDatabase.setFirebaseListener(this);
        mDatabase.getData();
    }

    /**
     * OVERRIDE METHODS
     */
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        setupMap();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    @Override
    public void onChildAdded(FirebaseUserModel model) {
        listIndex.add(model.getKey());
        int index = listIndex.indexOf(model.getKey());
        listModel.add(index, model);

        if (model.getUserCoords() != null) {
            marker = mMapsCustomizerManager.customizeMarker(model.getUserCoords(), sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_MARKER, getString(R.string.default_marker)));
            listMarker.add(index, marker);
            mGoogleMap = mMapsCustomizerManager.centerCamera(listMarker);
            mGoogleMap = mMapsCustomizerManager.customWindowInfo(listModel);
        }

        recyclerLocationAdapter.notifyDataSetChanged();

    }

    @Override
    public void onChildChanged(FirebaseUserModel model) {
        int index = listIndex.indexOf(model.getKey());

        if (index < listMarker.size()) {
            marker = listMarker.get(index);
        }

        if (model.getName() != null) {
            listModel.get(index).setName(model.getName());
        }

        if (model.getAddress() != null) {
            listModel.get(index).setAddress(model.getAddress());
        }

        if (model.getStatus() != null) {
            listModel.get(index).setStatus(model.getStatus());
        }

        if (model.getSafeStatus() != null) {
            listModel.get(index).setSafeStatus(model.getSafeStatus());
        }

        if (model.getUserCoords() != null) {
            listModel.get(index).setUserCoords(model.getUserCoords());
            if (index < listMarker.size()) {
                marker.remove();
                listMarker.remove(index);
            }
            try {
                marker = mMapsCustomizerManager.customizeMarker(new LatLng(model.getUserCoords().latitude, model.getUserCoords().longitude), sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_MARKER, getString(R.string.default_marker)));
                listMarker.add(index, marker);
            } catch (Exception e) {

            }
        }

        recyclerLocationAdapter.notifyDataSetChanged();
    }

    @Override
    public void onChildRemoved(FirebaseUserModel model) {
        int index = listIndex.indexOf(model.getKey());
        marker = listMarker.get(index);
        if (marker.getPosition().equals(model.getUserCoords())) {
            listIndex.remove(index);
            listModel.remove(index);
            listMarker.remove(index);
            marker.remove();
        }

        recyclerLocationAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClicked(String key) {
        if (key != null) {
            int index = listIndex.indexOf(key);
            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            marker = listMarker.get(index);
            mGoogleMap = mMapsCustomizerManager.setCurrentPosition(marker.getPosition(), 15);
        }
    }

    /**
     * SUPPORT METHODS
     */
    private MapView setupMap() {
        mapStyle = sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_MAP_STYLE, getString(R.string.default_style));
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMap = googleMap;
                mMapsCustomizerManager.setGoogleMap(mGoogleMap);
                mGoogleMap = mMapsCustomizerManager.customizeMapStyle(mapStyle);
                mGoogleMap = mMapsCustomizerManager.setCurrentPosition(defaultPosition, 11);
            }
        });
        return mMapView;
    }


}
