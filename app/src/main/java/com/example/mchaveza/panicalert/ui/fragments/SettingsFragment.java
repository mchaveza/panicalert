package com.example.mchaveza.panicalert.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.dd.CircularProgressButton;
import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.ui.bases.BaseFragment;
import com.example.mchaveza.panicalert.ui.dialogs.DialogDefaultBuilder;
import com.example.mchaveza.panicalert.ui.utils.ArgumentsConstants;
import com.example.mchaveza.panicalert.ui.utils.FirebaseStorageManager;
import com.github.jorgecastilloprz.FABProgressCircle;
import com.github.jorgecastilloprz.listeners.FABProgressListener;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mchaveza on 14/08/2017.
 */

public class SettingsFragment extends BaseFragment implements FirebaseStorageManager.StorageCallback, FABProgressListener {

    /**
     * BINDED RESOURCES
     **/
    @BindView(R.id.setup_cover)
    ImageView ivCover;

    @BindView(R.id.settings_show_hide_icon)
    ImageView ivDropdownIcon;

    @BindView(R.id.settings_change_profile)
    ImageView ivChangeProfile;

    @BindView(R.id.settings_show_hide_icon_marker)
    ImageView ivDropdownIconMarker;

    @BindView(R.id.settings_profile_image)
    CircleImageView ivProfileImage;

    @BindView(R.id.setup_name)
    TextView tvName;

    @BindView(R.id.settings_show_hide_styles_label)
    TextView tvShowHideStyles;

    @BindView(R.id.setup_message)
    TextView tvMessage;

    @BindView(R.id.settings_show_hide_label_marker)
    TextView tvShowHideMarker;

    @BindView(R.id.setup_map_style)
    TextView tvMapStyle;

    @BindView(R.id.setup_marker)
    TextView tvMarker;

    @BindView(R.id.setup_name_edit)
    EditText etName;

    @BindView(R.id.setup_message_edit)
    EditText etMessage;

    @BindView(R.id.setup_save_name)
    CircularProgressButton cpbSaveName;

    @BindView(R.id.setup_save_message)
    CircularProgressButton cpbSaveMessage;

    @BindView(R.id.setup_save_map_style)
    CircularProgressButton cpbSaveMapStyle;

    @BindView(R.id.setup_save_marker)
    CircularProgressButton cpbSaveMarker;

    @BindView(R.id.aubergine_check)
    CheckBox cbAubergine;

    @BindView(R.id.dark_check)
    CheckBox cbDark;

    @BindView(R.id.night_check)
    CheckBox cbNight;

    @BindView(R.id.silver_check)
    CheckBox cbSilver;

    @BindView(R.id.default_check)
    CheckBox cbDefaultMapStyle;

    @BindView(R.id.retro_check)
    CheckBox cbRetro;

    @BindView(R.id.red_marker_check)
    CheckBox cbRed;

    @BindView(R.id.green_marker_check)
    CheckBox cbGreen;

    @BindView(R.id.green_blue_marker_check)
    CheckBox cbGreenBlue;

    @BindView(R.id.yellow_marker_check)
    CheckBox cbYellow;

    @BindView(R.id.blue_marker_check)
    CheckBox cbBlue;

    @BindView(R.id.default_marker_check)
    CheckBox cbDefaultMarker;

    @BindView(R.id.settings_fab)
    FloatingActionButton fabUpload;

    private ExpandableLayout elExpandCollapseStyles;
    private ExpandableLayout elExpandCollapseMarker;
    private FABProgressCircle fabProgressCircle;

    /**
     * INSTANCES OF CLASSES
     **/
    private DialogDefaultBuilder mDialog;
    private FirebaseStorageManager mStorage;
    private static SettingsFragment instance;

    /**
     * SYSTEM VARIABLES
     **/

    public static SettingsFragment newInstance() {
        if (instance == null) {
            instance = new SettingsFragment();
        }
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        mDialog = new DialogDefaultBuilder(getActivity());
        elExpandCollapseStyles = (ExpandableLayout) view.findViewById(R.id.settings_expandable);
        elExpandCollapseMarker = (ExpandableLayout) view.findViewById(R.id.settings_expandable_marker);
        mStorage = new FirebaseStorageManager(getActivity());
        fabProgressCircle = (FABProgressCircle) view.findViewById(R.id.settings_upload);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fabProgressCircle.attachListener(this);

        mStorage.setListener(this);
        mStorage.downloadData(sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_CURRENT_KEY));

        GlideDrawableImageViewTarget targetCover = new GlideDrawableImageViewTarget(ivCover);
        Glide.with(this)
                .load(R.drawable.setup)
                .into(targetCover);

        if ((sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_USER_NAME) == null)) {
            tvName.setVisibility(View.GONE);
            etName.setVisibility(View.VISIBLE);
            etName.setText("");
            cpbSaveName.setVisibility(View.VISIBLE);
        } else {
            tvName.setText(sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_USER_NAME));
        }

        etName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if (nameIsOk()) {
                        sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_USER_NAME, etName.getText().toString().trim());
                        etName.setVisibility(View.GONE);
                        tvName.setVisibility(View.VISIBLE);
                        tvName.setText(etName.getText().toString().trim());
                    }
                }

                return false;
            }
        });

        etMessage.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if (messageIsOk()) {
                        sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_USER_MESSAGE, etMessage.getText().toString().trim());
                        etMessage.setVisibility(View.GONE);
                        tvMessage.setVisibility(View.VISIBLE);
                    }
                }
                return false;
            }
        });

        tvMapStyle.setText(sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_MAP_STYLE, getString(R.string.default_style)));
        tvMarker.setText(sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_MARKER, getString(R.string.default_marker)));
        tvMessage.setText(sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_USER_MESSAGE, getString(R.string.settings_default_message)));
        sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_USER_MESSAGE, tvMessage.getText().toString().trim());

    }

    @OnClick(R.id.setup_name)
    public void hideName() {
        tvName.setVisibility(View.GONE);
        etName.setVisibility(View.VISIBLE);
        cpbSaveName.setVisibility(View.VISIBLE);
        if (sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_USER_NAME) == null) {
            etName.setText("");
        } else {
            etName.setText(sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_USER_NAME));
        }
    }

    @OnClick(R.id.setup_message)
    public void hideMessage() {
        tvMessage.setVisibility(View.GONE);
        etMessage.setVisibility(View.VISIBLE);
        cpbSaveMessage.setVisibility(View.VISIBLE);
        if (sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_USER_MESSAGE) == null) {
            etMessage.setText(getString(R.string.settings_default_message));
        } else {
            etMessage.setText(sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_USER_MESSAGE));
        }
    }

    @OnClick(R.id.setup_save_name)
    public void setCpbSaveName() {
        cpbSaveName.setIndeterminateProgressMode(true);
        cpbSaveName.setProgress(50);
        etName.setEnabled(false);
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        if (nameIsOk()) {
                            cpbSaveName.setProgress(100);
                            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_USER_NAME, etName.getText().toString().trim());

                            new Handler().postDelayed(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            etName.setVisibility(View.GONE);
                                            tvName.setVisibility(View.VISIBLE);
                                            tvName.setText(etName.getText().toString().trim());
                                            cpbSaveName.setVisibility(View.GONE);
                                            cpbSaveName.setProgress(0);
                                            etName.setEnabled(true);
                                        }
                                    },
                                    1300
                            );
                        } else {
                            cpbSaveName.setProgress(-1);
                            etName.setEnabled(true);
                            new Handler().postDelayed(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            cpbSaveName.setProgress(0);
                                        }
                                    },
                                    700
                            );
                        }
                    }
                },
                850
        );
    }

    @OnClick(R.id.setup_save_message)
    public void setCpbSaveMessage() {
        cpbSaveMessage.setIndeterminateProgressMode(true);
        cpbSaveMessage.setProgress(50);
        etMessage.setEnabled(false);
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        if (messageIsOk()) {
                            cpbSaveMessage.setProgress(100);
                            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_USER_MESSAGE, etMessage.getText().toString().trim());

                            new Handler().postDelayed(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            etMessage.setVisibility(View.GONE);
                                            tvMessage.setVisibility(View.VISIBLE);
                                            tvMessage.setText(etMessage.getText().toString().trim());
                                            cpbSaveMessage.setVisibility(View.GONE);
                                            cpbSaveMessage.setProgress(0);
                                            etMessage.setEnabled(true);
                                        }
                                    },
                                    1300
                            );
                        } else {
                            cpbSaveMessage.setProgress(-1);
                            etMessage.setEnabled(true);
                            new Handler().postDelayed(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            cpbSaveMessage.setProgress(0);
                                        }
                                    },
                                    700
                            );

                        }
                    }
                },
                850
        );
    }

    @OnClick(R.id.setup_save_map_style)
    public void saveMapStyle() {
        cpbSaveMapStyle.setIndeterminateProgressMode(true);
        cpbSaveMapStyle.setProgress(50);
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        if (onMapStyleCheckListener() == -4) {
                            cpbSaveMapStyle.setProgress(100);
                            getCheckedStyle();
                            tvMapStyle.setText(sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_MAP_STYLE, getString(R.string.default_style)));
                            new Handler().postDelayed(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            expandStylesLayout();
                                            cpbSaveMapStyle.setProgress(0);
                                        }
                                    },
                                    700
                            );
                        } else {
                            cpbSaveMapStyle.setProgress(-1);
                            mDialog.buildSnackbar(getString(R.string.settings_map_style_error_message), getView());
                            new Handler().postDelayed(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            cpbSaveMapStyle.setProgress(0);
                                        }
                                    },
                                    700
                            );
                        }
                    }
                },
                850
        );
    }

    @OnClick(R.id.setup_save_marker)
    public void saveMarker() {
        cpbSaveMarker.setIndeterminateProgressMode(true);
        cpbSaveMarker.setProgress(50);
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        if (onMarkerCheckListener() == -4) {
                            cpbSaveMarker.setProgress(100);
                            getCheckedMarker();
                            tvMarker.setText(sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_MARKER, getString(R.string.default_marker)));
                            new Handler().postDelayed(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            expandMarkerLayout();
                                            cpbSaveMarker.setProgress(0);
                                        }
                                    },
                                    700
                            );
                        } else {
                            cpbSaveMarker.setProgress(-1);
                            mDialog.buildSnackbar(getString(R.string.settings_marker_error_message), getView());
                            new Handler().postDelayed(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            cpbSaveMarker.setProgress(0);
                                        }
                                    },
                                    700
                            );
                        }
                    }
                },
                850
        );
    }

    @OnClick(R.id.settings_expand_touchme)
    public void expandStylesLayout() {
        if (elExpandCollapseStyles.isExpanded()) {
            elExpandCollapseStyles.collapse();
            animateRotation(ivDropdownIcon, -180.0f, 0.0f, 1000);
            tvShowHideStyles.setText(getString(R.string.settings_show_label));
        } else {
            elExpandCollapseStyles.expand();
            animateRotation(ivDropdownIcon, 0.0f, -180.0f, 1000);
            tvShowHideStyles.setText(getString(R.string.settings_hide_label));
        }
    }

    @OnClick(R.id.settings_expand_touchme_marker)
    public void expandMarkerLayout() {
        if (elExpandCollapseMarker.isExpanded()) {
            elExpandCollapseMarker.collapse();
            animateRotation(ivDropdownIconMarker, -180.0f, 0.0f, 1000);
            tvShowHideMarker.setText(getString(R.string.settings_show_marker_label));
        } else {
            elExpandCollapseMarker.expand();
            animateRotation(ivDropdownIconMarker, 0.0f, -180.0f, 1000);
            tvShowHideMarker.setText(getString(R.string.settings_hide_marker_label));
        }
    }

    private boolean nameIsOk() {
        boolean isOk = true;

        if (etName.getText().toString().trim().length() == 0) {
            etName.setError(getString(R.string.settings_error_name_empty));
            isOk = false;
        }

        if (!etName.getText().toString().trim().contains(" ")) {
            etName.setError(getString(R.string.settings_error_name_incomplete));
            isOk = false;
        }

        return isOk;
    }

    @OnClick(R.id.settings_change_profile)
    public void changeProfile() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1234);
    }

    @OnClick(R.id.settings_fab)
    public void onUploadImage() {
        ivProfileImage.setVisibility(View.INVISIBLE);
        fabProgressCircle.show();
        mStorage.storageData(ivProfileImage, sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_CURRENT_KEY));
    }

    private boolean messageIsOk() {
        boolean isOk = true;

        if (etMessage.getText().toString().trim().length() == 0) {
            etMessage.setError(getString(R.string.settings_error_name_empty));
            isOk = false;
        }

        if (!etMessage.getText().toString().trim().contains(" ")) {
            etMessage.setError(getString(R.string.settings_error_name_incomplete));
            isOk = false;
        }

        return isOk;
    }

    private int onMapStyleCheckListener() {
        int stylesChecked = 0;

        if (cbAubergine.isChecked()) {
            stylesChecked++;
        } else {
            stylesChecked--;
        }

        if (cbDark.isChecked()) {
            stylesChecked++;
        } else {
            stylesChecked--;
        }

        if (cbDefaultMapStyle.isChecked()) {
            stylesChecked++;
        } else {
            stylesChecked--;
        }

        if (cbNight.isChecked()) {
            stylesChecked++;
        } else {
            stylesChecked--;
        }

        if (cbRetro.isChecked()) {
            stylesChecked++;
        } else {
            stylesChecked--;
        }

        if (cbSilver.isChecked()) {
            stylesChecked++;
        } else {
            stylesChecked--;
        }

        return stylesChecked;
    }

    private int onMarkerCheckListener() {
        int markersChecked = 0;

        if (cbRed.isChecked()) {
            markersChecked++;
        } else {
            markersChecked--;
        }

        if (cbYellow.isChecked()) {
            markersChecked++;
        } else {
            markersChecked--;
        }

        if (cbGreen.isChecked()) {
            markersChecked++;
        } else {
            markersChecked--;
        }

        if (cbGreenBlue.isChecked()) {
            markersChecked++;
        } else {
            markersChecked--;
        }

        if (cbBlue.isChecked()) {
            markersChecked++;
        } else {
            markersChecked--;
        }

        if (cbDefaultMarker.isChecked()) {
            markersChecked++;
        } else {
            markersChecked--;
        }

        return markersChecked;
    }

    private void getCheckedStyle() {
        if (cbAubergine.isChecked()) {
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_MAP_STYLE, getString(R.string.aubergine_style));
        }

        if (cbDark.isChecked()) {
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_MAP_STYLE, getString(R.string.dark_style));
        }

        if (cbDefaultMapStyle.isChecked()) {
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_MAP_STYLE, getString(R.string.default_style));
        }

        if (cbNight.isChecked()) {
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_MAP_STYLE, getString(R.string.night_style));
        }

        if (cbRetro.isChecked()) {
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_MAP_STYLE, getString(R.string.retro_style));
        }

        if (cbSilver.isChecked()) {
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_MAP_STYLE, getString(R.string.silver_style));
        }
    }

    private void getCheckedMarker() {
        if (cbRed.isChecked()) {
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_MARKER, getString(R.string.red_marker));
        }

        if (cbYellow.isChecked()) {
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_MARKER, getString(R.string.yellow_marker));
        }

        if (cbGreen.isChecked()) {
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_MARKER, getString(R.string.green_marker));
        }

        if (cbGreenBlue.isChecked()) {
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_MARKER, getString(R.string.green_blue_marker));
        }

        if (cbBlue.isChecked()) {
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_MARKER, getString(R.string.blue_marker));
        }

        if (cbDefaultMarker.isChecked()) {
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_MARKER, getString(R.string.default_marker));
        }

    }

    private void animateRotation(View view, float fromAngle, float toAngle, int duration) {
        AnimationSet animSet = new AnimationSet(true);
        animSet.setInterpolator(new DecelerateInterpolator());
        animSet.setFillAfter(true);
        animSet.setFillEnabled(true);

        final RotateAnimation rotateAnimation = new RotateAnimation(fromAngle, toAngle,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);

        rotateAnimation.setDuration(duration);
        rotateAnimation.setFillAfter(true);

        animSet.addAnimation(rotateAnimation);
        view.startAnimation(animSet);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        ivProfileImage.setImageBitmap(bitmap);
                        ivChangeProfile.setVisibility(View.GONE);
                        fabProgressCircle.setVisibility(View.VISIBLE);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                mDialog.buildToast(getString(R.string.cancel), Toast.LENGTH_SHORT);
            }
        }
    }


    @Override
    public void onUploadFinish() {
        fabProgressCircle.beginFinalAnimation();
    }

    @Override
    public void onUploadFailed() {
        fabProgressCircle.hide();
        fabProgressCircle.setVisibility(View.GONE);
        ivChangeProfile.setVisibility(View.VISIBLE);
        mDialog.buildSnackbar(getString(R.string.settings_upload_failed), getView());
    }

    @Override
    public void onDownloadFinish(Uri uri) {
        Glide.with(getActivity())
                .load(uri)
                .into(ivProfileImage);
        mDatabase.uploadSingleItem(ArgumentsConstants.PARAM_USER_PATH, String.valueOf(uri));

    }

    @Override
    public void onDownloadFailed() {
        //mDialog.buildSnackbar(getString(R.string.settings_download_failed), getView());
    }

    @Override
    public void onFABProgressAnimationEnd() {
        mDialog.buildSnackbar(getString(R.string.settings_upload_completed), getView());
        ivProfileImage.setVisibility(View.VISIBLE);
    }

}
