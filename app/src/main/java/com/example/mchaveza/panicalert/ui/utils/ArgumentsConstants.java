package com.example.mchaveza.panicalert.ui.utils;

/**
 * Created by mchaveza on 14/08/2017.
 */

public class ArgumentsConstants {

    public static String PARAM_FIRST_TIME = "first-time";
    public static String PARAM_LOCATION_GRANTED = "location-permission";
    public static String PARAM_READ_STORAGE_GRANTED = "location-permission";
    public static String PARAM_CURRENT_KEY = "current-key";
    public static String PARAM_USER_NAME = "user-name";
    public static String PARAM_USER_MESSAGE = "user-message";
    public static String PARAM_USER_COORDS = "user-coords";
    public static String PARAM_MAP_STYLE = "map-style";
    public static String PARAM_USER_SAFE_STATUS = "user-safe-status";
    public static String PARAM_USER_STATUS = "user-status";
    public static String PARAM_USER_ADDRESS = "user-address";
    public static String PARAM_USER_PATH = "user-path";
    public static String PARAM_MARKER = "user-marker";
}
