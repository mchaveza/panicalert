package com.example.mchaveza.panicalert.ui.utils;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.data.model.FirebaseUserModel;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mchaveza on 15/08/2017.
 */

public class FirebaseDatabaseManager {

    /**
     * INSTANCES OF CLASSES
     **/
    public static FirebaseDatabaseManager instance;
    private Context mContext;
    private DatabaseReference mDatabase;
    private SharedPreferencesConfiguration sharedPreferences;
    private FirebaseCallback mListener;

    /**
     * SYSTEM VARIABLES
     **/
    private static String currentKey;

    public static FirebaseDatabaseManager getInstance(Context context) {
        if (instance == null) {
            instance = new FirebaseDatabaseManager(context);
        }
        return instance;
    }

    private FirebaseDatabaseManager(Context context) {
        mContext = context;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        sharedPreferences = new SharedPreferencesConfiguration(mContext);
        initializeKey();
    }

    public void broadcastInfo(FirebaseUserModel model) {
        HashMap<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(ArgumentsConstants.PARAM_USER_NAME, model.getName());
        dataMap.put(ArgumentsConstants.PARAM_USER_ADDRESS, model.getAddress());
        dataMap.put(ArgumentsConstants.PARAM_USER_COORDS, new LatLng(model.getUserCoords().latitude, model.getUserCoords().longitude));

        mDatabase.child(currentKey).updateChildren(dataMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()) {
                    Toast.makeText(mContext.getApplicationContext(), mContext.getString(R.string.location_insert_message_failed), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void uploadSingleItem(String param, String item) {
        HashMap<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(param, item);

        mDatabase.child(currentKey).updateChildren(dataMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()) {
                    Toast.makeText(mContext.getApplicationContext(), mContext.getString(R.string.location_insert_message_failed), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void uploadSingleItem(String param, String item, String group) {
        HashMap<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(param, item);

        mDatabase.child(group).child(currentKey).updateChildren(dataMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()) {
                    Toast.makeText(mContext.getApplicationContext(), mContext.getString(R.string.location_insert_message_failed), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void updateStatus(FirebaseUserModel model) {
        HashMap<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(ArgumentsConstants.PARAM_USER_STATUS, model.getStatus());
        dataMap.put(ArgumentsConstants.PARAM_USER_SAFE_STATUS, model.getSafeStatus());

        mDatabase.child(currentKey).updateChildren(dataMap);
    }

    public void updateSingleItem(String argument, String item) {
        HashMap<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(argument, item);

        mDatabase.child(currentKey).updateChildren(dataMap);
    }

    private void initializeKey() {
        currentKey = sharedPreferences.getStringPreference(ArgumentsConstants.PARAM_CURRENT_KEY);
        if (currentKey == null) {
            currentKey = mDatabase.push().getKey();
            sharedPreferences.setStringPreference(ArgumentsConstants.PARAM_CURRENT_KEY, currentKey);
        }
    }

    public boolean checkModel(FirebaseUserModel model) {
        boolean isOk = true;
        if (model == null) {
            isOk = false;
        }

        if ((model.getName() == null) || (model.getName().trim().isEmpty())) {
            isOk = false;
        }

        if ((model.getAddress() == null) || (model.getAddress().trim().isEmpty())) {
            isOk = false;
        }

        if ((model.getUserCoords() == null)) {
            isOk = false;
        }

        return isOk;
    }

    public void getData() {
        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                for (DataSnapshot message : dataSnapshot.getChildren()) {
                    if (message.getKey().toString().contains("user")) {
                        mListener.onChildAdded(fillModel(dataSnapshot));
                        return;
                    }
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                for (DataSnapshot message : dataSnapshot.getChildren()) {
                    if (message.getKey().toString().contains("user")) {
                        mListener.onChildChanged(fillModel(dataSnapshot));
                        return;
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                for (DataSnapshot message : dataSnapshot.getChildren()) {
                    if (message.getKey().toString().contains("user")) {
                        mListener.onChildRemoved(fillModel(dataSnapshot));
                        return;
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private FirebaseUserModel fillModel(DataSnapshot dataSnapshot) {
        FirebaseUserModel model = new FirebaseUserModel();
        model.setKey((String) dataSnapshot.getKey());
        for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
            String key = messageSnapshot.getKey();
            switch (key) {
                case "user-safe-status":
                    model.setSafeStatus((String) messageSnapshot.getValue());
                    break;

                case "user-status":
                    model.setStatus((String) messageSnapshot.getValue());
                    break;

                case "user-coords":
                    model.setUserCoords(new LatLng((Double) messageSnapshot.child("latitude").getValue(), (Double) messageSnapshot.child("longitude").getValue()));
                    break;

                case "user-name":
                    model.setName((String) messageSnapshot.getValue());
                    break;

                case "user-address":
                    model.setAddress((String) messageSnapshot.getValue());
                    break;

                case "user-path":
                    model.setUri((String) messageSnapshot.getValue());
            }
        }

        return model;
    }

    public void deleteSingleItem(String group) {
        mDatabase.child(group).child(currentKey).removeValue();
    }

    public void setFirebaseListener(FirebaseCallback listener) {
        mListener = listener;
    }

    public interface FirebaseCallback {

        void onChildAdded(FirebaseUserModel model);

        void onChildChanged(FirebaseUserModel model);

        void onChildRemoved(FirebaseUserModel model);
    }

}
