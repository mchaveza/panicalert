package com.example.mchaveza.panicalert.ui.utils;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by mchaveza on 19/07/2017.
 */

public class FirebaseInstanceIdService extends com.google.firebase.iid.FirebaseInstanceIdService {

    private static final String REG_TOKEN = "REG_TOKEN";

    @Override
    public void onTokenRefresh() {
        String recent_token = FirebaseInstanceId.getInstance().getToken();
        Log.d(REG_TOKEN, recent_token);
    }
}