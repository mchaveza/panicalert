package com.example.mchaveza.panicalert.ui.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.RemoteControlClient;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.ui.dialogs.DialogDefaultBuilder;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.concurrent.atomic.AtomicMarkableReference;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mchaveza on 28/08/2017.
 */

@SuppressWarnings("VisibleForTests")
public class FirebaseStorageManager {

    private Context mContext;
    private byte[] data;
    private StorageCallback mListener;
    private FirebaseStorage storage;
    private StorageReference storageReference;

    public FirebaseStorageManager(Context context) {
        mContext = context;
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReferenceFromUrl("gs://panicalert-f0683.appspot.com");
    }

    public void setListener(StorageCallback mListener) {
        this.mListener = mListener;
    }

    public void storageData(CircleImageView imageView, String key) {
        StorageReference imageReference = storageReference.child("profiles/" + key + "/profile.jpg");

        convertImageToBytes(imageView);

        UploadTask uploadTask = imageReference.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                mListener.onUploadFailed();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                mListener.onUploadFinish();
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d("Progress", "Progress " + taskSnapshot.getBytesTransferred());
            }
        });
    }

    public void downloadData(String key) {
        StorageReference imageReference = storageReference.child("profiles/" + key + "/profile.jpg");

        imageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                mListener.onDownloadFinish(uri);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                mListener.onDownloadFailed();
            }
        });
    }

    private void convertImageToBytes(CircleImageView imageView) {
        imageView.setDrawingCacheEnabled(true);
        imageView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        imageView.layout(0, 0, imageView.getMeasuredWidth(), imageView.getMeasuredHeight());
        imageView.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(imageView.getDrawingCache());

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        data = outputStream.toByteArray();
    }

    public interface StorageCallback {

        void onUploadFinish();

        void onUploadFailed();

        void onDownloadFinish(Uri uri);

        void onDownloadFailed();
    }
}
