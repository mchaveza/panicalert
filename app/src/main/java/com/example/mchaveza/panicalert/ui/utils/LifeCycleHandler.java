package com.example.mchaveza.panicalert.ui.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.data.model.FirebaseUserModel;

/**
 * Created by mchaveza on 18/08/2017.
 */

public class LifeCycleHandler implements Application.ActivityLifecycleCallbacks {
    private int resumed;
    private int paused;
    private int started;
    private int stopped;
    private Context mContext;
    private FirebaseDatabaseManager mFirebaseManager;
    private SharedPreferencesConfiguration sharedPreferences;


    public LifeCycleHandler(Context context) {
        mContext = context;
        mFirebaseManager = FirebaseDatabaseManager.getInstance(mContext);
        sharedPreferences = new SharedPreferencesConfiguration(mContext);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
        ++resumed;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        ++paused;
        String status;
        if (resumed > paused) {
            status = mContext.getString(R.string.offline);
        } else {
            status = mContext.getString(R.string.online);
        }
        mFirebaseManager.updateSingleItem(ArgumentsConstants.PARAM_USER_STATUS, status);
        android.util.Log.w("test", "application is in foreground: " + (resumed > paused));
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
        ++started;
    }

    @Override
    public void onActivityStopped(Activity activity) {
        ++stopped;
        String status;
        if (started > stopped) {
            status = mContext.getString(R.string.online);
        } else {
            status = mContext.getString(R.string.offline);
        }
        mFirebaseManager.updateSingleItem(ArgumentsConstants.PARAM_USER_STATUS, status);
        android.util.Log.w("test", "application is visible: " + (started > stopped));
    }

}
