package com.example.mchaveza.panicalert.ui.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mchaveza.panicalert.R;
import com.example.mchaveza.panicalert.data.model.FirebaseUserModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mchaveza on 24/08/2017.
 */

public class MapsCustomizerManager {

    /**
     * BINDED RESOURCES
     */

    /**
     * INSTANCES OF CLASSES
     */
    private Context mContext;
    private GoogleMap mGoogleMap;
    private CameraPosition cameraPosition;

    public MapsCustomizerManager(Context context) {
        mContext = context;
    }

    /**
     * SUPPORT METHODS
     */
    public void setGoogleMap(GoogleMap googleMap) {
        mGoogleMap = googleMap;
    }

    public GoogleMap customizeMapStyle(String style) {
        boolean success;
        try {
            switch (style) {
                case "Aubergine":
                    success = mGoogleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    mContext, R.raw.map_auberine));
                    if (!success) {
                        Log.e("Error", "Style parsing failed");
                    }
                    break;
                case "Dark":
                    success = mGoogleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    mContext, R.raw.map_dark));
                    if (!success) {
                        Log.e("Error", "Style parsing failed");
                    }
                    break;
                case "Night":
                    success = mGoogleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    mContext, R.raw.map_night));
                    if (!success) {
                        Log.e("Error", "Style parsing failed");
                    }
                    break;
                case "Retro":
                    success = mGoogleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    mContext, R.raw.map_retro));
                    if (!success) {
                        Log.e("Error", "Style parsing failed");
                    }
                    break;
                case "Silver":
                    success = mGoogleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    mContext, R.raw.map_silver));
                    if (!success) {
                        Log.e("Error", "Style parsing failed");
                    }
                    break;
                case "Default":
                    success = mGoogleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    mContext, R.raw.map_default));
                    if (!success) {
                        Log.e("Error", "Style parsing failed");
                    }
                    break;
            }

        } catch (Resources.NotFoundException e) {
            Log.e("Error", "Can't find style");
        }

        return mGoogleMap;
    }

    public Marker customizeMarker(LatLng location, String markerType) {
        @DrawableRes int resId = R.drawable.ic_custom_marker;
        switch (markerType){
            case "Default":
                resId = R.drawable.ic_custom_marker;
                break;
            case "Red marker":
                resId = R.drawable.ic_red_marker;
                break;
            case "Green marker":
                resId = R.drawable.ic_green_marker;
                break;
            case "Green-blue marker":
                resId = R.drawable.ic_green_blue_marker;
                break;
            case "Yellow marker":
                resId = R.drawable.ic_yellow_marker;
                break;
            case "Blue marker":
                resId = R.drawable.ic_blue_marker;
        }
        return mGoogleMap.addMarker(new MarkerOptions()
                .position(location)
                .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(resId))));
    }

    public Bitmap getMarkerBitmapFromView(@DrawableRes int resId) {
        View customMarkerView = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.custom_marker_icon);

        markerImageView.setImageResource(resId);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();

        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);

        Drawable drawable = customMarkerView.getBackground();

        if (drawable != null) {
            drawable.draw(canvas);
        }

        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

    public GoogleMap setCurrentPosition(LatLng position, int zoom) {
        cameraPosition = new CameraPosition.Builder().target(position).zoom(zoom).build();
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        return mGoogleMap;
    }

    public GoogleMap customWindowInfo(final List<FirebaseUserModel> listModel) {
        mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View customView = inflater.inflate(R.layout.custom_info_window, null);

            @Override
            public View getInfoWindow(Marker marker) {
                render(marker);
                return customView;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }

            public void render(Marker marker) {
                TextView infoName = (TextView) customView.findViewById(R.id.custom_name);
                TextView infoAddress = (TextView) customView.findViewById(R.id.custom_address);
                TextView infoStatus = (TextView) customView.findViewById(R.id.custom_status);
                TextView infoSafeStatus = (TextView) customView.findViewById(R.id.custom_safe_status);

                for (FirebaseUserModel model : listModel) {
                    if (marker.getPosition().equals(model.getUserCoords())) {
                        infoName.setText(model.getName());
                        infoAddress.setText(model.getAddress());
                        infoStatus.setText(model.getStatus());
                        infoSafeStatus.setText(model.getSafeStatus());
                    }
                }
            }
        });

        return mGoogleMap;
    }

    public GoogleMap centerCamera(List<Marker> listMarker) {
        double latitude = 0;
        double longitude = 0;
        for (Marker marker : listMarker) {
            latitude += marker.getPosition().latitude;
            longitude += marker.getPosition().longitude;
        }

        if (listMarker.size() != 0) {
            latitude = latitude / listMarker.size();
            longitude = longitude / listMarker.size();
        }

        cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(11).build();
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        return mGoogleMap;
    }


}
