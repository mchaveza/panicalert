package com.example.mchaveza.panicalert.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by knock on 05/05/2017.
 */

public class SharedPreferencesConfiguration {

    private Context mContext;
    private static SharedPreferences sharedPreferences;
    private static ArgumentsConstants args;

    public SharedPreferencesConfiguration(Context context) {
        args = new ArgumentsConstants();
        mContext = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public String getStringPreference(String stringPreference) {
        return sharedPreferences.getString(stringPreference, null);
    }

    public String getStringPreference(String stringPreference, String defaultValue) {
        return sharedPreferences.getString(stringPreference, defaultValue);
    }

    public Boolean getBooleanPreference(String stringPreference, Boolean defaultValue) {
        return sharedPreferences.getBoolean(stringPreference, defaultValue);
    }

    public int getIntBoolean(String stringPreference) {
        return sharedPreferences.getInt(stringPreference, -1);
    }

    public void setStringPreference(String preferenceName, String preference) {
        sharedPreferences.edit().putString(preferenceName, preference).commit();
        sharedPreferences.edit().putString(preferenceName, preference).apply();
    }

    public void setBooleanPreference(String preferenceName, Boolean preference) {
        sharedPreferences.edit().putBoolean(preferenceName, preference).commit();
        sharedPreferences.edit().putBoolean(preferenceName, preference).apply();
    }

    public void setIntBoolean(String preferenceName, int preference) {
        sharedPreferences.edit().putInt(preferenceName, preference).apply();
        sharedPreferences.edit().putInt(preferenceName, preference).commit();
    }

}
